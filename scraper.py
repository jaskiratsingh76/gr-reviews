import xml.etree.ElementTree as ElementTree
from bs4 import BeautifulSoup
import json
import urllib

class GRScraper(object):
	def __init__(self, key, title, author=None):
		self.title = title
		self.author = author
		self.key = key

	def get_reviews_json(self):
		# construct rest url
		rest_url = self.get_rest_url()
		# fetch xml page
		reviews_xml = self.get_page(rest_url)
		# parse xml to extract iframe link
		iframe_link = self.get_iframe_link(reviews_xml)
		# check if link exists
		if iframe_link == None:
			return None
		# fetch html of iframe
		reviews_html = self.get_page(iframe_link)
		# parse html to get json
		parser = HTMLParser(reviews_html)
		# return json
		return parser.get_reviews()

	def get_rest_url(self):
		params = {'title': self.title, 'key':self.key}
		# include author if not None
		if self.author != None:
			params['author'] = self.author
		# encode and construct url
		rest_url = "https://www.goodreads.com/book/title.xml?" + urllib.urlencode(params)
		return rest_url

	def get_page(self, url):
		# fetches a page and returns it's contents
		return urllib.urlopen(url).read()

	def get_iframe_link(self, reviews_xml):
		# extract reviews iframe link from xml
		try:
			scraper = XMLScraper(reviews_xml)
			return scraper.get_iframe_link()
		except:
			return None

class HTMLParser(object):
	def __init__(self, html):
		# soupify page
		self.soup = BeautifulSoup(html)

	def get_iframe_link(self):
		return self.soup.find('iframe', id='the_iframe')['src']
		
	def get_reviews(self):
		# extract reviews div
		reviews_div = self.soup.body.find_all('div', itemprop="reviews", limit=10)
		
		# init list
		list_of_reviews = []

		# extracting reviews
		for review in reviews_div:
			author_html = review("span", class_="gr_review_by")
			review_html = review("div", class_="gr_review_text")

			# extract review author
			author_str = ""
			for item in author_html:
				item = item.stripped_strings
				author_str = " ".join(item)
			try:
				author_str = str(author_str)
			except:
				continue

			# extract review content
			review_str = ""
			for item in review_html:
				item = item.stripped_strings
				review_str = " ".join(item)
				review_str = review_str.replace(" ...more", "")
			try:
				review_str = str(review_str)
			except:
				continue

			# extract review link
			link_str = ""
			for item in review_html:
				item = item("link", {"itemprop": "url"})
				for link in item:
					link_str = link["href"]

			# construct dict
			rev = {"author": author_str, "link": link_str, "review": review_str}

			# add to list of dicts
			list_of_reviews.append(rev)

		# construct json string
		review_count = len(list_of_reviews)
		reviews_dict = {"count": review_count, "reviews": list_of_reviews}

		# generate json string
		reviews_json = json.dumps(reviews_dict)
		return reviews_json

class XMLScraper(object):
	def __init__(self, reviews_xml):
		self.root = ElementTree.fromstring(reviews_xml)

	def get_iframe_link(self):
		for widget in self.root.iter('reviews_widget'):
			widget_html = widget.text

		soup = HTMLParser(widget_html)
		return soup.get_iframe_link()

scraper = GRScraper("7AsRot6ilGexGeVKdCREA", "Norwegian Wood", "Haruki Murakami")
reviews = scraper.get_reviews_json()