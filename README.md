Goodreads Reviews Scraper
-------------------------

Goodreads doesn't return it's reviews in `JSON`, and rather returns a link to a messy `iframe` that we can embed in our application.  

No sir, we don't want your `iframe`.

This is a very basic scraper that returns the reviews in `JSON`.

Usage
-----
www.gr-reviews.appspot.com/reviews.json?title=title[&author=author]  
Reviews are currently limited to ASCII format only.

Dependencies  
------------
BeautifulSoup